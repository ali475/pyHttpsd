# Introduction
Occasionally I need to create a certificate for testing purposes. Certificates have a lot of uses beyond securing web traffic.

This blog will describe the steps for starting a secure web server.  We will need to do the following:

* Create or request a certificate
* Start a simple web server and use the certificate to start HTTPS

## Creating a private key and certificate signing request (CSR)
The following command will create a private key named __domain.key__ and a CSR named __domain.csr__.
```
# openssl req -newkey rsa:2048 -nodes -keyout domain.key -out domain.csr -subj /C=US/ST=Massachusetts/L=Boston/O=Smallmoon/CN=smallmoon.com
Generating a 2048 bit RSA private key
......+++
...........+++
writing new private key to 'domain.key'
# 
```

## Create a self-signed certificate
The following command uses the CSR from above and the private key and creates a self-signed certificate.
```
# openssl x509 -signkey domain.key -in domain.csr -req -days 365 -out domain.crt
Signature ok
subject=/C=US/ST=Massachusetts/L=Boston/O=Smallmoon/CN=smallmoon.com
Getting Private key
# 
```

## Keep the files safe

We now have some important files.

### domain.key
Keep this file very safe and make it readable only by root
```
# chmod 400 domain.key
```

### domain.csr
This file is not needed anymore since we have self signed it and saved it away.  You can disregard domain.csr.

### domain.crt
This is the public certificate.  Keep it safe, but it can be shared publicly.

## Starting HTTPS
Create a file named pyHttpsd and fill it with these lines:
```
#!/usr/bin/env python
import BaseHTTPServer, SimpleHTTPServer
import ssl

# Uncomment this line and comment out the next line if we wish to 
# only run on localhost
#
# httpd = BaseHTTPServer.HTTPServer(('localhost', 4443),
#        SimpleHTTPServer.SimpleHTTPRequestHandler)
httpd = BaseHTTPServer.HTTPServer(('', 4443),
       SimpleHTTPServer.SimpleHTTPRequestHandler)

httpd.socket = ssl.wrap_socket (httpd.socket,
        keyfile='domain.key',
        certfile='domain.crt', server_side=True)

httpd.serve_forever()
```

Now make the file executable and see how it works by pointing our browser to port 4443.
```
# chmod a+x pyHttpsd
# ./pyHttpsd

```

One very cool thing about this python web server is that it is running as a normal non-root user.  Now we are going to run this server as a daemon and continue to avoid running as root.

## Daemonizing HTTPS
In this section, we will put the server in the background and then use an iptables trick to run the server on the privileged standard port 443/tcp.  We will do this without running our pyHttpd server as root.

Let us first put the server in the background and protect it from us logging out. Also we want to move this server to a sub directory.
```
# mkdir httpRoot
# cd httpRoot
# nohup ../pyHttpsd > ../pyHttpsd.out &
Ignoring stdin, redirecting stderr to stdout

#
```
Now we can use iptables to redirect port 443 to port 4443.  Thus we can "listen" on a low numbered privileged port without being root.
```
# sudo  iptables -t nat -A PREROUTING -i eth0 -p tcp --dport 443 -j REDIRECT --to-port 4443
```

# A more robust pyHttpsd
After playing around for a little bit, I enhanced the server to use argparse so I could specify different certificates and the HTML root directory.
The directory _cert_ was added to hold the different certificates.

## Enhanced pyHttpsd
The code looks like so once we were done:
```
...
class pyHTTPSServer(object):

    """
    Sample implementation of a python HTTP server using the ssl wrap module
    """

    def __init__(self):
        self.keyfile = 'domain.key'
        self.certfile = 'domain.crt'
        self.port = 4443
        self.server_address = ''
        self.html_root = None

    def run(self):
        
        ...
        
        httpd.socket = ssl.wrap_socket (httpd.socket,
                                        keyfile=self.keyfile,
                                        certfile=self.certfile,
                                        server_side=True)

        try:
            os.chdir(self.html_root)
        except OSError:
            print 'Failed to change directory to ' + args.htmlroot

        httpd.serve_forever()
        # We return here if we are killed
        return 0


def main():
    parser = argparse.ArgumentParser(description='python simple HTTPS server')
    parser.add_argument('--keyfile', help='path to private key file')
    parser.add_argument('--certfile', help='path to public certificate file')
    parser.add_argument('--htmlroot', help='path to the html directory tree')
    parser.add_argument('--verbose', '-v', action = 'store_true', help='path to the html directory tree')
    args = parser.parse_args()

    httpsd = pyHTTPSServer()
    if args.keyfile:
        httpsd.keyfile = args.keyfile

    if args.certfile:
        httpsd.certfile = args.certfile

    if args.htmlroot:
        httpsd.html_root = args.htmlroot

    if args.verbose:
        print 'Certificate is ' + httpsd.certfile
        print 'Keyfile is '+ httpsd.keyfile
        print 'HTML directory is ' + httpsd.html_root

    # The run method runs until we are killed
    httpsd.run()

    return 0

if __name__ == '__main__':
    main()
    sys.exit(0)

```

Now, to kick this program off we just needed to do this:
```
$ pyHttpsd --htmlroot=htmlRoot --keyfile=cert/domain.key --certfile=cert/domain.crt -v
Certificate is cert/domain.crt
Keyfile is cert/domain.key
HTML directory is htmlRoot

```
