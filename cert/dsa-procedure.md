DSA Certificate Creation
====
Create a DSA Parameters File
----
```openssl dsaparam 1024 < /dev/random > dsaparam.pem```

Create a private key
----
```openssl gendsa dsaparam.pem -out dsadomain.key```

Create a CSR
----
```openssl req -new -key dsadomain.key -out dsadomain.csr -subj /C=US/ST=Massachusetts/L=Boston/O=Smallmoon/CN=smallmoon.com```

Create the Public Certificate
----
```openssl x509 -signkey dsadomain.key -in dsadomain.csr -req -days 730 -out dsadomain.crt```

