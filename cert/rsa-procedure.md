RSA Certificate Creation
====

Create a RSA based CSR and private key
----
```openssl req -newkey rsa:2048 -nodes -keyout domain.key -out domain.csr -subj /C=US/ST=Massachusetts/L=Boston/O=Smallmoon/CN=smallmoon.com```

Create a Public Certificate
----
```openssl x509 -signkey domain.key -in domain.csr -req -days 730 -out domain.crt```

