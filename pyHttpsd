#!/usr/bin/env python
#
# Copyright (c) 2017 Shaheen H. Ali
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

import BaseHTTPServer, SimpleHTTPServer
import argparse
import ssl
import sys
import os

class pyHTTPSServer(object):

    """
    Sample implementation of a python HTTP server using the ssl wrap module
    """

    def __init__(self):
        self.keyfile = 'domain.key'
        self.certfile = 'domain.crt'
        self.port = 4443
        self.server_address = ''
        self.html_root = '.'
        self.ssl_version = ssl.PROTOCOL_TLSv1

    def get_ssl_version(self):
        return {ssl.PROTOCOL_TLSv1 : 'TLSv1',
                ssl.PROTOCOL_TLSv1_1 : 'TLSv1.1',
                ssl.PROTOCOL_TLSv1_2 : 'TLSv1.2'}.get(self.ssl_version, 'TLSv1.2')

    def run(self):
        # type: () -> object

        # Uncomment this line and comment out the next line if we wish to
        # only run on localhost
        #
        # httpd = BaseHTTPServer.HTTPServer(('localhost', self.port),
        #        SimpleHTTPServer.SimpleHTTPRequestHandler)
        httpd = BaseHTTPServer.HTTPServer((self.server_address, self.port),
                                          SimpleHTTPServer.SimpleHTTPRequestHandler)
        httpd.socket = ssl.wrap_socket (httpd.socket,
                                        keyfile=self.keyfile,
                                        certfile=self.certfile,
                                        server_side=True,
                                        ssl_version=self.ssl_version)

        try:
            os.chdir(self.html_root)
        except OSError:
            print 'Failed to change directory to ' + self.html_root
            return 0

        httpd.serve_forever()
        # We return here if we are killed
        return 0

def main():
    parser = argparse.ArgumentParser(description='python simple HTTPS server')
    parser.add_argument('--keyfile', '-k', help='path to private key file')
    parser.add_argument('--certfile', '-c', help='path to public certificate file')
    parser.add_argument('--docroot', '-d', help='path to the html directory tree')
    parser.add_argument('--verbose', '-v', action = 'store_true', help='print helpful debug output')
    parser.add_argument('--tls', help='specify TLS version (e.g. 1.1, 1.2).  default is 1.0')
    parser.add_argument('--port', '-p', help='specify the TCP port to listen on')

    args = parser.parse_args()

    httpsd = pyHTTPSServer()
    if args.keyfile:
        httpsd.keyfile = args.keyfile

    if args.certfile:
        httpsd.certfile = args.certfile

    if args.docroot:
        httpsd.html_root = args.docroot

    if args.tls == '1.2':
        httpsd.ssl_version = ssl.PROTOCOL_TLSv1_2
    if args.tls == '1.1':
        httpsd.ssl_version = ssl.PROTOCOL_TLSv1_1
    elif args.tls == 1:
        httpsd.ssl_version = ssl.PROTOCOL_TLSv1

    if args.verbose:
        print 'Certificate is ' + httpsd.certfile
        print 'Keyfile is '+ httpsd.keyfile
        print 'HTML directory is ' + httpsd.html_root
        print 'SSL version = ' + httpsd.get_ssl_version()

    if args.port:
        httpsd.port = int(args.port)

    try:
        # The run method runs until we are killed
        httpsd.run()
        rc = 1
    except KeyboardInterrupt:
        print 'Exiting'
        rc = 0

    return rc

if __name__ == '__main__':

    sys.exit(main())

